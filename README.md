# Инструкция по запуску S3App через Docker

1. Установите Docker: https://docs.docker.com/engine/install/
1. git clone https://gitlab.com/s3app-team/s3app/s3app-automation
1. При необходимости отредактируйте docker-compose.yml
    1. Если хотите включить подтверждение по email-у, укажите реквизиты электронной почты и удалите S3APP_NO_CONFIRMATION
1. Запустите docker-compose up --build в папке куда склонировали репозиторий
1. Веб-интерфейс S3App будет доступен на localhost:3000
